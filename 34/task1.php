<?php
if ($_GET['MODE'] == 1) {
    session_name('FIRST_COUNTER');
    session_start();
} elseif ($_GET['MODE'] == 2) {
    session_name('SECOND_COUNTER');
    session_start();
} elseif ($_GET['MODE'] != 1 & $_GET['MODE'] != 2) {
    echo "Ошибка! Передано некорректное значение GET параметра MODE";
}
if ($_GET['DELETE_SESSION_DATA'] == 'Y') {
    $_SESSION = [];
}
if (!isset($_SESSION['COUNTER'])) $_SESSION['COUNTER'] = 0;
$_SESSION['COUNTER'] = $_SESSION['COUNTER'] + 1;
echo $_SESSION['COUNTER'].' '.session_id().' '.session_name().' '.session_save_path();