<?php
function loadLibrary1($className)
{
    $pathToClass = __DIR__ . "/library1/src/tools/".basename($className).".php";
    if(file_exists($pathToClass))
    {
        require_once ($pathToClass);
    }
}

function loadLibrary2($className)
{
    require_once (__DIR__ .'/library2/LIB2Test3.php');
}

function loadLibrary3($className)
{
    require_once (__DIR__.'/additional_libraries/library3/classes/main/LIB3Test4.php');
}

spl_autoload_register('loadLibrary1');
spl_autoload_register('loadLibrary2');
spl_autoload_register('loadLibrary3');

use library1\general\LIB1Test2;
use library1\tools\LIB1Test1;
use library2\LIB2Test3;
use library3\main\LIB3Test4;

$obj1 = new LIB1Test1;
$obj2 = new LIB1Test2;
$obj3 = new LIB2Test3;
$obj4 = new LIB3Test4;

echo $obj1->print().'<br />';
echo $obj2->print().'<br />';
echo $obj3->print().'<br />';
echo $obj4->print().'<br />';
