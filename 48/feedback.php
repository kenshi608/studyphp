<?php
require_once("connect.php");

try {
    if (empty($_POST['name'])) exit('Не заполнено поле "Имя"');
    if (empty($_POST['position'])) exit('Не заполнено поле "Должность"');
    if (empty($_POST['email'])) exit('Не заполнено поле "Email"');
    if (empty($_POST['review_text'])) exit('Не заполнено поле "Текст отзыва"');

    $query = "INSERT INTO reviews VALUES (NULL, NOW(), :name, :position, :email, :review_text)";
    $reviews = $pdo->prepare($query);
    $reviews->execute(['name' => $_POST['name'], 'position' => $_POST['position'], 'email' => $_POST['email'], 'review_text' => $_POST['review_text']]);
    $rnd = time();
    header("Location: http://{$_SERVER['SERVER_NAME']}/form.php?$rnd");
    exit();
    
} catch (PDOException $e) {
    echo "Ошибка выполнения запроса: " . $e->getMessage();
}
?>