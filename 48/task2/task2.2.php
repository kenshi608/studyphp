<?php

function doHardWorkBySteps($iStepNumber)
{
  sleep(15);

  file_put_contents(
    'messages.log',
    date("d.m.Y H:i:s") . " Успешно закончен шаг номер " . $iStepNumber . "\n\n",
    FILE_APPEND
  );

  if($iStepNumber > 15)
  {
    return true;
  }
  else
  {
    return false;
  }
}

if (!isset($_GET['step'])) {
  $step = 1;
}  
else 
  $step = $_GET['step'];


header("Status: 200 OK");
$dir = dirname($_SERVER['SCRIPT_NAME']);
if ($dir == '\\') $dir = '';

while (!doHardWorkBySteps($step)) {
  $step++;
  header("Location: $dir/task2.2.php?step=$step");
  exit();
}

echo "Функция закончила работу";