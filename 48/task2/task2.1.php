<?php

function doHardWorkBySteps($iStepNumber)
{
  sleep(15);

  file_put_contents(
    'messages.log',
    date("d.m.Y H:i:s") . " Успешно закончен шаг номер " . $iStepNumber . "\n\n",
    FILE_APPEND
  );

  if($iStepNumber > 15)
  {
    return true;
  }
  else
  {
    return false;
  }
}

if (!isset($_GET['step'])) {
  $step = 1;
}  
else 
  $step = $_GET['step'];

while (!doHardWorkBySteps($step)) {
  $step++;
  header("Location: http://{$_SERVER['SERVER_NAME']}/task2/task2.1.php?step=$step");
  exit();
}

echo "Функция закончила работу";