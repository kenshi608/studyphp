<?php

function contractChecker ($str) {
    if (preg_match('/^\d{2}-\d{3}-\d{3,}\/\d{5,}/', $str)) 
        echo "Корректный номер договора<br>";
    else echo "Некорректный номер договора<br>";
}

$str1 = "22-222-2222/222222";
$str2 = "22-222-2222222222";
$str3 = "22-222-22/222222";
$str4 = "22-222-222/22222222222222";

contractChecker($str1);
contractChecker($str2);
contractChecker($str3);
contractChecker($str4);