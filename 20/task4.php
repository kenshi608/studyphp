<?php

function letterChecker ($str) {
    if (preg_match("/^[\w]+$/", $str)) 
        echo "Строка содержит только символы латинских букв, цифр и знака подчеркивания<br>";
    else echo "Строка содержит лишние символы<br>";
}

$str1 = "abct875_";
$str2 = 'abc t875_';
$str3 = "56гаф_";
letterChecker($str1);
letterChecker($str2);
letterChecker($str3);