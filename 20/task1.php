<?php

function SnilsChecker ($str) {
    if (!preg_match("/^\d{3}-\d{3}-\d{3}-\d{2}$/", $str)) 
        echo "Некорректный номер СНИЛС<br>";
    else echo "Корректный номер СНИЛС<br>";
}

$str1 = "123-456-789-12";
$str2 = "123-6-789-12";
$str3 = "123-12";

SnilsChecker($str1);
SnilsChecker($str2);
SnilsChecker($str3);