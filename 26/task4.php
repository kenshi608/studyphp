<?php
class RecursionException extends Exception {
    public function __construct()
    {
        parent::__construct('Рекурсия сломалась!');
    }
}

function factorial($num) {
    if ($num = 1) {
        throw new RecursionException;
    }
    return $num * factorial($num - 1);
}

try {
    echo factorial(5);
} catch (RecursionException $exp) {
    echo $exp.'<hr>';
    echo $exp->getTraceAsString().'<hr>';
    echo 'Файл: '.$exp->getFile().' Строка: '.$exp->getLine();
}