<?php

function errorFunc1() {
    echo __FUNCTION__;
}

function errorFunc2() {
    echo __FUNCTION__;
}

set_error_handler('errorFunc1', E_ALL);
set_error_handler('errorFunc2', E_ALL);

//trigger_error("пользовательская ошибка (1)");
restore_error_handler();
//trigger_error("пользовательская ошибка (2)");
restore_error_handler();
trigger_error("пользовательская ошибка (3)");