<?php
ini_set("error_reporting", E_ALL);
ini_set("log_errors", 1);
ini_set("error_log", "php-error.log");
ini_set("display_errors", 0);

abstract class Animal
{
    abstract function move();
}

class Dog extends Animal
{
    function move(){
        return "Собака бежит";
    }
}

class Kangaroo extends Animal
{
    function move(){
        return "Кенгуру прыгает";
    }
}

class Pigeon extends Animal
{
    function move(){
        return "Голубь летит";
    }
}

$array = [new Cat, new DateTime(), new Kangaroo, new Pigeon];

foreach ($array as $obj) {
    if ($obj instanceof Animal)
        echo $obj->move()."<br />";
    else 
        echo "Объект не является наследником класса Animal<br />";
}