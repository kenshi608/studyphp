<?php
require_once "http_build_url.php";
$sUrl = "http://user@example.com:80/test/path/?arg=value";

$parsed = parse_url($sUrl);

if ($parsed['path'] === "/path/test/") {
    parse_str(@$parsed['query'], $query);
    $query = array();
    $query['param1'] = '123';
    $query['param2'] = '012';
    $parsed['query'] = http_build_query($query);
    $sUrl = http_build_url($parsed);
}

elseif ($parsed['path'] === "/test/path/") {
    parse_str(@$parsed['query'], $query);
    $query = array();
    $query['param123'] = 1;
    $query['param012'] = 2;
    $parsed['query'] = http_build_query($query);
    $sUrl = http_build_url($parsed);
}

echo $sUrl;

