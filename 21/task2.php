<?php

function visitLog() {
    chdir(dirname(__FILE__));
    $file = fopen('visit_log.txt', 'a');
    fwrite($file, date("d.m.Y H:i:s")."\n");
    fclose($file);
}

register_shutdown_function('visitLog');
exit();