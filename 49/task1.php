<?php

// "404 131 131 404 789 061 061 789 404 131 131 404 789 061 061 789 404 131 131 404"
function printNumber($iNumber)
{
    echo $iNumber;
}

function printSpace()
{
    echo " ";
}

function blockOne()
{
    printNumber('404');
    printSpace();
    printNumber('131');
    printSpace();
    printNumber('131');
    printSpace();
    printNumber('404');
    printSpace();
}

function blockTwo()
{
    printNumber('789');
    printSpace();
    printNumber('061');
    printSpace();
    printNumber('061');
    printSpace();
    printNumber('789');
    printSpace();
}

$i = 0;
ob_start();
while($i != 3) {
    if ($i == 2) {
        blockOne();
        $i++;
        continue; 
    } 
    blockOne();
    blockTwo();
    $i++;
}
$str = ob_get_contents();
ob_end_flush();