<?php
require_once('connect.php');

$query = "SELECT count(*) FROM reviews";
$rev = $pdo->query($query);
$rowCount = $rev->fetch();
$pageCount = ceil($rowCount[0]/3);
if (is_null($_GET['offset']))
    $currentOffset = 0;
else
    $currentOffset = $_GET['offset'];

$query = "SELECT * FROM reviews ORDER BY name LIMIT $currentOffset, 3";
$rev = $pdo->query($query);
try {
    while($reviews = $rev->fetch())
        echo $reviews['review_date'].' '.$reviews['name'].' '.$reviews['position'].' '.$reviews['email'].' '.$reviews['review_text'].'<br />';
} catch (PDOException $e) {
    echo "Ошибка выполнения запроса: " . $e->getMessage();
}

$offset = 0;
for($i=1;$i<=$pageCount;$i++) {
    echo '<a href="'.basename(__FILE__).'?offset='.$offset.'">'.$i.'</a> ';
    $offset += 3;
 }

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script>
        $(document).ready(function(){
	        $('#Mybtn').click(function(){
  		        $('#MyForm').toggle();
            });
        });
    </script>
</head>
<body>
    <table id="MyForm">
        <form  action="feedback.php" method="POST">
            <tr>
                <td>Имя:</td>
                <td><input type="text" name="name"></td>
            </tr>
            <tr>
                <td>Должность:</td>
                <td><input type="text" name="position"></td>
            </tr>
            <tr>
                <td>Email:</td>
                <td><input type="text" name="email"></td>
            </tr>
            <tr>
                <td>Текст отзыва:</td>
                <td><textarea name="review_text" rows="10" cols="40"></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Добавить отзыв"></td>
            </tr>
        </form>
    </table>
    <a id="Mybtn" href="#">Показать/скрыть форму</a>
</body>
</html>