<?php
require_once("connect.php");

try {
    var_dump($_POST);
    $query = "INSERT INTO reviews VALUES (NULL, NOW(), :name, :position, :email, :review_text)";
    $reviews = $pdo->prepare($query);
    $reviews->execute(['name' => $_POST['name'], 'position' => $_POST['position'], 'email' => $_POST['email'], 'review_text' => $_POST['review_text']]);
} catch (PDOException $e) {
    echo "Ошибка выполнения запроса: " . $e->getMessage();
}
?>