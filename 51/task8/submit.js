$(document).ready(function() {
    $('form').submit(function(event) {
        event.preventDefault();
        var name = $('#name').val();
        var position = $('#position').val();
        var email = $('#email').val();
        var review_text = $('#review_text').val();

        if (name == '' || position == '' || email == '' || review_text == '') {
            alert('Заполнены не все поля!');
        } else {
            $.ajax({
                method: "POST",
                url: "feedback.php",
                data: $(this).serialize(),

                success: function(){
                    alert('Данные сохранены');
                }
            });
        }
    });
});