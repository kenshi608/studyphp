var rectangle = {
    width: 10,
    height: 15,
    getWidth: function() {
        return this.width;
    },
    getHeight: function() {
        return this.height;
    },
    getArea: function() {
        return this.width * this.height;
    }
};

console.log('Ширина: ' + rectangle.getWidth());
console.log('Длина: ' + rectangle.getHeight());
console.log('Площадь: ' + rectangle.getArea());