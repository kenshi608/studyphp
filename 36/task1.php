<?php
class Sanitizer {
    public static function sanitize(array $data, array $args) {
        $format = [];
        foreach ($args as $options) {
            $format[$options['NAME']] = $options['FORMAT'];
        }
        return filter_var_array($data, $format);
    }
}

class Validator {
    public static function validate(array $data, array $args) {
        $format = [];
        foreach ($args as $options) {
            $format[$options['NAME']] = $options['FORMAT'];
        }
        $arr = filter_var_array($data, $format);
        foreach ($arr as $key => $value) {
            if ($value === false & $key != 'Bool') {
                return ["SUCCESS" => 'false', "WRONG_FIELD_NAME" => $key];
            }
        } 
        return ['SUCCESS' => 'true'];
    }
}

$Sanitizer_DATA = ['Email' => 'john.doe@example.c om', 
'Encode' => 'params=Привет мир!', 
'Slash' => 'Symbol \\',
'Float' => 'rt45.13rt',
'Int' => '@546r',
'SpecialChars' => '<h1> & </h1>',
'SpecialChars2' => '<h1> & </h1>',
'URL' => 'https://www.php.net/manual/ru/filter.��filters.sanitize.php'];

$Sanitizer_Format = [["NAME" => 'Email', "FORMAT" => FILTER_SANITIZE_EMAIL],
["NAME" => 'Slash', "FORMAT" => FILTER_SANITIZE_ADD_SLASHES],
["NAME" => 'Float', "FORMAT" => [
    'filter' => FILTER_SANITIZE_NUMBER_FLOAT,
    'flags' => FILTER_FLAG_ALLOW_FRACTION]
],
["NAME" => 'Int', "FORMAT" => FILTER_SANITIZE_NUMBER_INT],
["NAME" => 'SpecialChars', "FORMAT" => FILTER_SANITIZE_SPECIAL_CHARS],
["NAME" => 'SpecialChars2', "FORMAT" => FILTER_SANITIZE_FULL_SPECIAL_CHARS],
["NAME" => 'URL', "FORMAT" => FILTER_SANITIZE_URL]];

$Validator_DATA = ['Bool' => 'yes',
'Email' => 'john.doe@example.com',
'Float' => '45.13',
'Int' => '15',
'IP' => '192.168.0.1',
'RegExp' => 'ch10',
'URL' => 'https://www.php.net/manual/ru/filter.filters.validate.php'];

$Validator_Format = [["NAME" => 'Bool', "FORMAT" => FILTER_VALIDATE_BOOLEAN],
["NAME" => 'Email', "FORMAT" => FILTER_VALIDATE_EMAIL],
["NAME" => 'Float', "FORMAT" => FILTER_VALIDATE_FLOAT],
["NAME" => 'Int', "FORMAT" => FILTER_VALIDATE_INT],
["NAME" => 'IP', "FORMAT" => [
    'filter' => FILTER_VALIDATE_IP,
    'flags' => FILTER_FLAG_IPV4]
],
["NAME" => 'RegExp', "FORMAT" => [
    'filter' => FILTER_VALIDATE_REGEXP,
    'options' => ['regexp' => '/^ch\d+$/']]
],
["NAME" => 'URL', "FORMAT" => FILTER_VALIDATE_URL]];

echo '<pre>';
print_r(Sanitizer::sanitize($Sanitizer_DATA, $Sanitizer_Format));
echo '</pre>';

echo '<pre>';
print_r(Validator::validate($Validator_DATA, $Validator_Format));
echo '</pre>';

