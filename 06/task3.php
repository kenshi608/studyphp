<?php
$sTwentyFiveWithSpace = "25 ";
$dEightPointTwentyFiveHundredths = 8.25;
$sEmptyString = "";
$bFalse = false;
$iZero = 0;
settype($sTwentyFiveWithSpace,"boolean");
settype($dEightPointTwentyFiveHundredths,"boolean");
settype($sEmptyString,"boolean");
settype($bFalse,"boolean");
settype($iZero,"boolean");
echo $sTwentyFiveWithSpace."\n";
echo $dEightPointTwentyFiveHundredths."\n";
echo $sEmptyString."\n";
echo $bFalse."\n";
echo $iZero."\n";
echo (int)$iZero;