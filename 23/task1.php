<?php
abstract class Animal
{
    abstract function move();
}

class Dog extends Animal
{
    function move(){
        return "Собака бежит";
    }
}

class Kangaroo extends Animal
{
    function move(){
        return "Кенгуру прыгает";
    }
}

class Pigeon extends Animal
{
    function move(){
        return "Голубь летит";
    }
}

$array = [new Dog, new DateTime(), new Kangaroo, new Pigeon];

foreach ($array as $obj) {
    if ($obj instanceof Animal)
        echo $obj->move()."<br />";
    else 
        echo "Объект не является наследником класса Animal<br />";
}