<?php

function fMath($x) {
    // sin(pi*x*exp(x^2))^2 - (1/2)*x^(1/2)
    return pow(sin(pi()*$x*exp($x * $x)),2) - 1/2*sqrt($x);
}

echo fMath(0) + fMath(0.5) + fMath(0.7);