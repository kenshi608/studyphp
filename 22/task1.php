<?php 
class rectangle
{
    private $length;
    private $width;
    public function __construct($startLength = 2, $startWidth = 1) {
        $this->length = $startLength;
        $this->width = $startWidth;
    }

    public function getArea() {
        return $this->length * $this->width;
    }

    public function setLength($newLength) {
        $this->length = $newLength;
    }

    public function setWidth($newWidth) {
        $this->width = $newWidth;
    }

    public function __toString()
    {
        return "Прямоугольник с длинной {$this->length} и шириной {$this->width}<br />";
    }
}

$obFirstRectangleVariable = new rectangle();
$obSecondRectangleVariable = $obFirstRectangleVariable;
$obThirdRectangleVariable = clone $obFirstRectangleVariable;

$obFirstRectangleVariable->setLength(7);
$obThirdRectangleVariable->setLength(8);

echo $obFirstRectangleVariable;
echo $obSecondRectangleVariable;
echo $obThirdRectangleVariable;