<?php 
function recursionSumm(int $iNumber)
{
    if ($iNumber <= 0) return 0;
    else return $iNumber + recursionSumm($iNumber-1);
}
echo recursionSumm(100);