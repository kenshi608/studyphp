<?php
function nextElement()
{
    $arr = ["test1", "test2", "test3", "test4", "test5"];
    static $i = 0;
    $currentElement = $arr[$i];
    if ($i == 4) $i = 0;
    else $i++;
    return $currentElement;
}

for ($i=0; $i < 10; $i++) echo nextElement()." ";