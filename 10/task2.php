<?php
    $arArrayOfArrays = [
        [1, -2, 0, 3],
        [-2, 5, -7],
        [3],
        [0, 0, -1]
    ];
    
    $arSumElements = [];
    foreach ($arArrayOfArrays as $ar)
        $arSumElements[] = array_sum($ar);
    sort($arSumElements);

    $sortedArrayOfArrays = [];
    for ($i=0; $i < count($arArrayOfArrays); $i++) {
        for ($j=0; $j < count($arArrayOfArrays); $j++) {
            if ($arSumElements[$i] == array_sum($arArrayOfArrays[$j])) {
              $sortedArrayOfArrays[] = $arArrayOfArrays[$j];  
            } 
        }
    }
    echo '<pre>';
    print_r($sortedArrayOfArrays);
    echo '</pre>';
